<?php

class Cart {

    /**
     * Массив-хранилище для продуктов.
     *
     * @var array $storage
     */
    public $storage = [];

    /**
     * Добавление продукта в корзину.
     *
     * @param Product $product
     * @return void
     */
    public function add($product)
    {
        $this->storage[] = $product;
    }

    /**
     * Удаление продукта по имени из корзины.
     *
     * @param string $name
     * @return void
     */
    public function remove($name)
    {
        $newStorage = [];

        foreach ($this->storage as $product) {
            if($product->name !== $name) {
                $newStorage[] = $product;
            }
        }

        $this->storage = $newStorage;
    }

    /**
     * Подсчёт стоимости товаров в корзине.
     *
     * @return float|integer
     */
    public function getTotal()
    {
        $sum = 0;

        foreach($this->storage as $product) {
            $sum += $product->price;
        }

        return $sum;
    }

    /**
     * Подсчёт количества товаров в корзине.
     *
     * @return integer
     */
    public function getCount()
    {
        return count($this->storage);
    }
}
